# Sujet Spécial TP1


## Membres du groupe :

* Basile Bonicel
* Mathias Durand
* Théo Loubet

## Questions :

*Quel protocole choisi-t-on pour le chat du jeu vidéo ? UDP ou TCP et pourquoi ?*
Dans le cadre d'un jeu vidéo en ligne possédant un chat textuel. On voudra séparer les serveurs du jeu et le chat afin de gérer les deux séparément et gagner en performance.
On pourra alors utiliser le protocole de son choix pour le jeu, UDP dans la majorité des cas à part pour des jeux moins intense comme les jeux en tour par tour pour lesquels on pourrait opter pour du TCP.
Au niveau du chat, l'usage du protocole TCP est idéal puisqu'il permet s'assurer de la bonne réception du message par le destinataire.
De plus si on sépare le chat du jeu alors la latence plus élevé du TCP comparé à l'UDP n'est plus un problème.
