#pragma once

#include <string>
#include <windows.h>
#include <winsock2.h>
#include "Connection.hpp"
namespace uqac::network
{
	class UDPConnection : public Connection
	{
		public:
			UDPConnection(SOCKET sock);
			~UDPConnection();
			void Send(std::string msg) override;
			std::string Receive() override;
			void Shutdown() override;
			std::string GetId() override;
	};
}