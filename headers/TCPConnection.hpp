#pragma once

#include <string>
#include <windows.h>
#include <winsock2.h>
#include "Connection.hpp"
namespace uqac::network
{
	class TCPConnection : public Connection
	{
		public:
			TCPConnection(SOCKET sock);
			~TCPConnection();
			void Send(std::string msg) override;
			std::string Receive() override;
			void Shutdown() override;
			std::string GetId() override;
	};
}