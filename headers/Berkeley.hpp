#pragma once
#include <string>
#include <vector>
#include <thread>
#include <functional>
#include "Connection.hpp"
#include "Terminal.hpp"

#define DEFAULT_PORT "56789"
namespace uqac::network
{
	enum class ConnectionType
	{
		UDP,
		TCP
	};

	class Berkeley {
		public :
			Berkeley(ConnectionType type, std::function<void(Berkeley *berk, Connection *, std::string)> f_new_message_in, std::function<void(Berkeley *berk, Connection *)> f_new_connection_in, std::function<void(Berkeley *berk, std::string)> f_connection_lost);
			~Berkeley();
			void ListenForClient(std::string port = DEFAULT_PORT);
			Connection * ConnectToServer(std::string address, std::string port = DEFAULT_PORT);
			void BroadcastMessageToAllButSender(std::string message, Connection *co);
			void Run(std::string port = DEFAULT_PORT);
			bool runThread = true;
			bool IsVectorOfConnectionsEmpty();

		private :
			void Update();
			void ListenForIncomingMessages();
			ConnectionType connectionType;
			std::vector<Connection *> vectorOfConnections;
			Terminal *updateTerminal_ptr = nullptr;
			std::thread updateThread;
			SOCKET GetSocket(std::string address, std::string port = DEFAULT_PORT);
			addrinfo * GetAddressInfo(std::string port, std::string address = "");
			std::function<void(Berkeley *berk, Connection *, std::string)> f_new_message;
			std::function<void(Berkeley *berk, Connection *)> f_new_connection;
			std::function<void(Berkeley *berk, std::string)> f_connection_lost;
	};
}