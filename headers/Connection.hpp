#pragma once
#include <windows.h>
#include <winsock2.h>
#include <string>
namespace uqac::network
{
	class LostConnectionError : public std::exception
	{
		public:
			std::string socketname;
			LostConnectionError(std::string sock)
			{
				socketname = sock;
			}
			virtual const std::string message() const throw()
			{
				return socketname + " s'est déconnecté";
			}
			virtual const std::string connectionIdLost() const throw()
			{
				return socketname;
			}
	};

	class Connection
	{
		public:
			virtual void Send(std::string msg) = 0;
			virtual void Shutdown() = 0;
			virtual std::string Receive() = 0;
			virtual std::string GetId() = 0;
			friend class Berkeley;
		protected:
			static inline unsigned int currentConnectionId = 0;
			unsigned int connectionId;
			SOCKET clientSocket;
			static const int bufferSize = 512;
			char bufferReceive[bufferSize];
	};
}