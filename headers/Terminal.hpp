#pragma once
#include "TCPConnection.hpp"
namespace uqac::network
{
	class Terminal
		{
		public:
			Terminal() {};
			Terminal(SOCKET sock);
			~Terminal() {};
			TCPConnection *ListenForIncomingConnections();

		private:
			SOCKET listeningSocket;
			addrinfo *GetAddressInfo(std::string port);
	};
}