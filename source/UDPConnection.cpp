#pragma once

#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <windows.h>
#include <winsock2.h>
#include "UDPConnection.hpp"
namespace uqac::network
{
    UDPConnection::UDPConnection(SOCKET sock)
    {
        clientSocket = sock;
        connectionId = ++currentConnectionId;
    }

    UDPConnection::~UDPConnection()
    {
        closesocket(clientSocket);
        WSACleanup();
    }

    void UDPConnection::Shutdown()
    {
        int iResult = shutdown(clientSocket, SD_SEND);
        if (iResult == SOCKET_ERROR)
        {
            std::cout << "[E] Extinction de la connexion echouee avec l'erreur : " << WSAGetLastError() << std::endl;
        }
    }

    std::string UDPConnection::GetId()
    {
        return std::to_string(connectionId);
    }

    void UDPConnection::Send(std::string msg)
    {
        int iResult = sendto(clientSocket, msg.c_str(), (int) msg.length(), 0, nullptr, 0);
        if (iResult == SOCKET_ERROR)
        {
            std::cout << "[E] Envoi echoue avec l'erreur : " << WSAGetLastError() << std::endl;
        }
    }
    std::string UDPConnection::Receive()
    {
        std::string tmp = "";
        int iResult = recvfrom(clientSocket, bufferReceive, bufferSize, 0, nullptr, nullptr);
        if (iResult > 0)
        {
            for (int i = 0; i < iResult; i++)
            {
                tmp += bufferReceive[i];
            }
        }
        else if (iResult < 0 && WSAGetLastError() == WSAECONNRESET)
        {
            std::string tmp = std::to_string((int)clientSocket);
            Shutdown();
            throw LostConnectionError(GetId());
        }
        else if (iResult <= 0)
            std::cout << "[E] Reception echouee avec l'erreur : " << WSAGetLastError() << std::endl;
        return tmp;
    }
}