#define WIN32_LEAN_AND_MEAN

#include "Terminal.hpp"
#include "TCPConnection.hpp"
//#include "UDPConnection.h"
#include <winsock2.h>
#include <windows.h>

#include <iostream>
#include <string>

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Mswsock.lib")
#pragma comment(lib, "AdvApi32.lib")
namespace uqac::network
{
	TCPConnection *Terminal::ListenForIncomingConnections()
	{
		SOCKET ClientSocket = INVALID_SOCKET;

		fd_set readingSetAccept;
		FD_ZERO(&readingSetAccept);
		struct timeval tvl = {0, 50};
		FD_SET(listeningSocket, &readingSetAccept);
		int ret = select(0, &readingSetAccept, nullptr, nullptr, &tvl);

		if (ret > 0)
		{
			if (FD_ISSET(listeningSocket, &readingSetAccept))
			{
				//std::cout << "demande de connection..." << std::endl; --> message déplacé à la callback
				// Accept a client socket
				ClientSocket = accept(listeningSocket, NULL, NULL);
			}
		}
		return ((ClientSocket != INVALID_SOCKET) ? new TCPConnection(ClientSocket) : nullptr);
	}

	Terminal::Terminal(SOCKET ListenSocket)
	{
		listeningSocket = ListenSocket;
	}
}