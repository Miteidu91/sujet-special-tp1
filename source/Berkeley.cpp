#define WIN32_LEAN_AND_MEAN

#include <string>
#include <iostream>
#include <functional>
#include <thread>
#include <algorithm>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "Connection.hpp"
#include "TCPConnection.hpp"
#include "UDPConnection.hpp"
#include "Berkeley.hpp"
#include "Terminal.hpp"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Mswsock.lib")
#pragma comment(lib, "AdvApi32.lib")

#define DEFAULT_PORT "56789"
namespace uqac::network
{
    Berkeley::Berkeley(ConnectionType type, std::function<void(Berkeley *berk, Connection *, std::string)> f_new_message_in, std::function<void(Berkeley *berk, Connection *)> f_new_connection_in, std::function<void(Berkeley *berk, std::string)> f_connection_lost_in)
    {
        connectionType = type;
        f_new_message = f_new_message_in;
        f_new_connection = f_new_connection_in;
        f_connection_lost = f_connection_lost_in;
        WSADATA wsaData;
        // Initialize Winsock
        int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0)
        {
            std::cout << "[E] WSAStartup failed with error: " << iResult << std::endl;
            exit(1);
        }
    }

    bool Berkeley::IsVectorOfConnectionsEmpty()
    {
        return vectorOfConnections.empty();
    }

    // Fonction destinée à un usage serveur
    void Berkeley::ListenForClient(std::string port)
    {
        SOCKET ListenSocket = INVALID_SOCKET;
        int iResult;

        addrinfo *result = GetAddressInfo(port);
        // Create a SOCKET for connecting to server
        ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (ListenSocket == INVALID_SOCKET)
        {
            std::cout << "[E] socket failed with error: " << WSAGetLastError() << std::endl;
            freeaddrinfo(result);
            WSACleanup();
            exit(1);
        }
        // Setup the listening socket
        iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);

        if (iResult == SOCKET_ERROR)
        {
            std::cout << "[E] bind failed with error: " << WSAGetLastError() << std::endl;
            freeaddrinfo(result);
            closesocket(ListenSocket);
            WSACleanup();
            exit(1);
        }
        freeaddrinfo(result);

        if(connectionType == ConnectionType::TCP) 
        {
            iResult = listen(ListenSocket, SOMAXCONN);
            if (iResult == SOCKET_ERROR)
            {
                std::cout << "[E] listen failed with error: " << WSAGetLastError() << std::endl;
                closesocket(ListenSocket);
                WSACleanup();
                exit(1);
            }
            updateTerminal_ptr = new Terminal(ListenSocket);
        }
    }

    addrinfo *Berkeley::GetAddressInfo(std::string port, std::string address)
    {
        int iResult;
        struct addrinfo *result = NULL;
        struct addrinfo hints;

        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = (connectionType == ConnectionType::TCP) ? SOCK_STREAM : SOCK_DGRAM;
        hints.ai_protocol = (connectionType == ConnectionType::TCP) ? IPPROTO_TCP : IPPROTO_UDP;
        hints.ai_flags = AI_PASSIVE;

        // Resolve the server address and port
        iResult = getaddrinfo((strcmp(address.c_str(), "") != 0) ? address.c_str() : NULL, port.c_str(), &hints, &result);
        if (iResult != 0)
        {
            std::cout << "[E] getaddrinfo failed with error: " << iResult << std::endl;
            WSACleanup();
            exit(1);
        }
        return result;
    }

    SOCKET Berkeley::GetSocket(std::string address, std::string port)
    {
        addrinfo *result = GetAddressInfo(port, address);
        SOCKET ConnectSocket = INVALID_SOCKET;
        struct addrinfo *ptr = NULL;
        int iResult;
        // Attempt to connect to an address until one succeeds
        for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
        {

            // Create a SOCKET for connecting to server
            ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                                ptr->ai_protocol);
            if (ConnectSocket == INVALID_SOCKET)
            {
                std::cout << "[E] socket failed with error: " << WSAGetLastError() << std::endl;
                WSACleanup();
                exit(1);
            }

            if (connectionType == ConnectionType::TCP)
            {
                // Connect to server.
                iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
                if (iResult == SOCKET_ERROR)
                {
                    closesocket(ConnectSocket);
                    ConnectSocket = INVALID_SOCKET;
                    continue;
                }
            }
            break;
        }
        
        freeaddrinfo(result);
        if (ConnectSocket == INVALID_SOCKET)
        {
            std::cout << "[E] Unable to connect to server!" << std::endl;
            WSACleanup();
            exit(1);
        }
        return (ConnectSocket);
    }

    // Fonction destinée à un usage client
    Connection *Berkeley::ConnectToServer(std::string address, std::string port)
    {
        Connection* c = nullptr;
        SOCKET s = GetSocket(address, port);
        if (connectionType == ConnectionType::TCP)
        {
            c = new TCPConnection(s);
        }
        else if (connectionType == ConnectionType::UDP)
        {
            c = new UDPConnection(s);
        }
        else
        {
            std::cout << "[E] invalid connection type" << std::endl;
            exit(1);
        }
        std::cout << "[I] Connexion creee" << std::endl;
        vectorOfConnections.push_back(c);
        return c;
    }

    void Berkeley::Update()
    {
        if(updateTerminal_ptr)
        {
            Connection *newco = updateTerminal_ptr->ListenForIncomingConnections();
            if(newco)
            {
                vectorOfConnections.push_back(newco);
                f_new_connection(this, newco);
            }
        }
        if(vectorOfConnections.size() > 0) ListenForIncomingMessages();
    }

    void Berkeley::BroadcastMessageToAllButSender(std::string message, Connection *co)
    {
        for(Connection* c : vectorOfConnections)
        {
            if(c->clientSocket != co->clientSocket)
            {
                //std::cout << "[I] message transmis a " << c->clientSocket << std::endl;
                c->Send(message);
            }
        }
    }

    //parametre pour les tests
    void Berkeley::ListenForIncomingMessages()
    {
        std::string message_received = "";
        fd_set readingSet;
        FD_ZERO(&readingSet);
        struct timeval tvl = {0, 50};
        for (Connection *co : vectorOfConnections)
        {
            FD_SET(co->clientSocket, &readingSet);
        }
        int activity = select(0, &readingSet, nullptr, nullptr, &tvl);

        if (activity > 0)
        {
            for (Connection *co : vectorOfConnections)
            {
                if (FD_ISSET(co->clientSocket, &readingSet))
                {
                    try
                    {
                        message_received = co->Receive();
                        if (message_received != "")
                        {
                            f_new_message(this, co, message_received);
                        }
                    }
                    catch(LostConnectionError& e)
                    {
                        vectorOfConnections.erase(std::remove(vectorOfConnections.begin(), vectorOfConnections.end(), co), vectorOfConnections.end());
                        f_connection_lost(this, e.connectionIdLost());
                        delete co;
                    }
                }
            }
        }
    }

    void Berkeley::Run(std::string port)
    {
        updateThread = std::thread([this](){while(runThread){Update();}});
    }

    Berkeley::~Berkeley()
    {
        runThread = false;
        updateThread.join();
        delete updateTerminal_ptr;
        for (Connection* co : vectorOfConnections)
        {
            co->Shutdown();
            delete co;
        }
    }
}