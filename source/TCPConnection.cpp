#pragma once

#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <windows.h>
#include <winsock2.h>
#include "../Headers/TCPConnection.hpp"
namespace uqac::network
{
	TCPConnection::TCPConnection(SOCKET sock)
	{
		clientSocket = sock;
		connectionId = ++currentConnectionId;
	}

	TCPConnection::~TCPConnection()
	{
		closesocket(clientSocket);
		WSACleanup();
	}

	void TCPConnection::Shutdown()
	{
		int iResult = shutdown(clientSocket, SD_SEND);
		if (iResult == SOCKET_ERROR)
		{
			std::cout << "[E] Extinction de la connexion echouee avec l'erreur : " << WSAGetLastError() << std::endl;
		}
	}

	std::string TCPConnection::GetId()
	{
		return std::to_string(connectionId);
	}

	void TCPConnection::Send(std::string msg)
	{
		int iResult = send(clientSocket, msg.c_str(), (int)strlen(msg.c_str()), 0);
		if (iResult == SOCKET_ERROR)
		{
			std::cout << "[E] Envoi echoue avec l'erreur : " << WSAGetLastError() << std::endl;
		}
	}

	std::string TCPConnection::Receive()
	{
		std::string tmp = "";
		int iResult;
		iResult = recv(clientSocket, bufferReceive, bufferSize, 0);
		if (iResult > 0){
			for(int i = 0; i<iResult; i++){
				tmp += bufferReceive[i];
			}
		}
		else if (iResult < 0 && WSAGetLastError() == WSAECONNRESET)
		{
			std::string tmp = std::to_string((int)clientSocket);
			Shutdown();
			throw LostConnectionError(GetId());
		}
		else if (iResult <= 0)
			std::cout << "[E] Reception echouee avec l'erreur : " << WSAGetLastError() << std::endl;
		return tmp;
	}
}