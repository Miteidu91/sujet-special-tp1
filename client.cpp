#define WIN32_LEAN_AND_MEAN

#include "Connection.hpp"
#include "Berkeley.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>

void onReceiveMessage(uqac::network::Berkeley *berk, uqac::network::Connection *co, std::string message_received)
{
    std::cout << message_received << std::endl;
}

void onNewConnection(uqac::network::Berkeley *berk, uqac::network::Connection *co)
{
}

void onConnectionLost(uqac::network::Berkeley *berk, std::string co)
{
    std::cout << "[I] Connection avec le serveur perdue" << std::endl;
    if (berk->IsVectorOfConnectionsEmpty())
    {
        std::cout << "[I] Fermeture du programme" << std::endl;
        berk->runThread = false;
        exit(0);
    }
}

int __cdecl main(int argc, char **argv)
{
    // Validate the parameters
    if (argc < 3)
    {
        std::cout << "usage: " << argv[0] << " <server ip> <port>" << std::endl;
        exit(1);
    }
    uqac::network::ConnectionType protocol = uqac::network::ConnectionType::TCP;
    std::string ip = argv[1];
    std::string port = argv[2];
    uqac::network::Berkeley berk(protocol, onReceiveMessage, onNewConnection, onConnectionLost);
    uqac::network::Connection *serverConnection = berk.ConnectToServer(ip, port);
    berk.Run();
    std::string msg;
    while (true)
    {
        std::getline(std::cin, msg);
        serverConnection->Send(msg);
        Sleep(1);
    }
    return 0;
}