#define WIN32_LEAN_AND_MEAN

#include "Connection.hpp"
#include "Berkeley.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include <iostream>

void onReceiveMessage(uqac::network::Berkeley *berk, uqac::network::Connection *co, std::string message_received)
{
    std::string sender = co->GetId();
    std::cout << sender << " dit : " << message_received << std::endl;
    std::string broadcast = co->GetId() + "> " + message_received;
    berk->BroadcastMessageToAllButSender(broadcast, co);
}

void onNewConnection(uqac::network::Berkeley *berk, uqac::network::Connection *co)
{
    std::cout << "[I] Nouvelle connection de " << co->GetId() << std::endl;
    std::string broadcast = co->GetId() + " s'est connecte au serveur !";
    berk->BroadcastMessageToAllButSender(broadcast, co);
}

void onConnectionLost(uqac::network::Berkeley *berk, std::string coText)
{
    std::cout << "[I] Connection avec " << coText << " perdue" << std::endl;
}

int __cdecl main(int argc, char **argv)
{
    if (argc < 2)
    {
        // Validate the parameters
        std::cout << "usage: " << argv[0] << " <port>" << std::endl;
        exit(1);
    }
    uqac::network::ConnectionType protocol = uqac::network::ConnectionType::TCP;
    std::string port = argv[1];
    uqac::network::Berkeley berk(protocol, onReceiveMessage, onNewConnection, onConnectionLost);
    berk.ListenForClient(port);
    berk.Run();
    std::cout << "[I] Serveur operationnel" << std::endl;
    while (true)
    {
        Sleep(1);
    }
    return 0;
}